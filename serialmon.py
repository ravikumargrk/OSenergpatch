import serial
from datetime import datetime

# Is it always COM3 ?
ser = serial.Serial('COM3',115200)
i = 1

def writeserial(szInput, wait = 1):
    ser.write(szInput.encode())
    reply = ''
    opens = 0
    closes = 0
    while len(reply)==0 or opens!=closes or ser.inWaiting():  
        c = ser.read()                     # Read reply byte
        c = c.decode('utf-8')              # convert to char
        if c=='{':
            opens = opens + 1
        if c=='}':
            closes = closes + 1    
        reply = reply + c                  # Append char to string
    print(reply)                           # Print reply
    return len(reply)                      # Return size of reply
    

while True:
    promptstr = " In["+ str(i) + "]: "
    szInput = input(promptstr)
    promptstr = "Out["+ str(i) + "]: "
    print(promptstr, end='')
    if len(szInput) > 0:
        writeserial(szInput)
    else:
        print("\n")
    i = i + 1 