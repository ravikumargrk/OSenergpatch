# Driver to control openscope

# do "pip install pyserial" as admin first !
import serial
import time

# How to replace this with USB/tty0 for linux based os ?
# added a comment
ser = serial.Serial('COM3',115200,timeout = 10)

def writeserial(szInput, wait = 1):
    ser.write(szInput.encode())
    reply = ''
    opens = 0
    closes = 0
    while len(reply)==0 or opens!=closes or ser.inWaiting():  
        c = ser.read()                     # Read reply byte
        if len(c)!=0:
            if ord(c) < 128:
                c = c.decode('utf-8')              # convert to char
                if c=='{':
                    opens = opens + 1
                if c=='}':
                    closes = closes + 1
                reply = reply + c                  # Append char to string    
    print(reply)                           # Print reply
    return len(reply)                      # Return size of reply

writeserial('{"mode":"JSON"}')

writeserial('{"device":[{"command":"calibrationLoad","type":"flash"}]}')

print('\n--------------------------------------------------\nSET PARAM')

writeserial('{"awg":{"1":[{"command":"setRegularWaveform","signalType":"sine","signalFreq":1000000,"vpp":3000,"vOffset":0}]},"osc":{"1":[{"command":"setParameters","vOffset":0,"gain":1,"sampleFreq":10000000,"bufferSize":30000,"triggerDelay":0}]},"trigger":{"1":[{"command":"setParameters","source":{"instrument":"osc","channel":1,"type":"risingEdge","lowerThreshold":0,"upperThreshold":100},"targets":{"osc":[1]}},{"command":"single"}]}}')

writeserial('{"awg":{"1":[{"command":"run"}]}}')

time.sleep(10)
print('\n--------------------------------------------------\nCHK#3')

writeserial('{"awg":{"1":[{"command":"getCurrentState"}]},"osc":{"1":[{"command":"getCurrentState"}]},"trigger": {"1": [{"command":"getCurrentState"}]}}')

print('\n--------------------------------------------------\nAQC')

getsize = writeserial('{"osc":{"1":[{"command":"read","acqCount":1}]}}')

time.sleep(2)
print('\n--------------------------------------------------\nSTOP')

writeserial('{"trigger":{"1":[{"command":"stop"}]},"awg":{"1":[{"command":"stop"}]}}')



