OpenScope v1.296.0
Written by: Keith Vogel, Digilent Inc.
Copyright 2016 Digilent Inc.

File Systems Initialized
MRF24 Info -- DeviceType: 0x2 Rom Version: 0x31 Patch Version: 0xC

USB+:     4825178uV
VCC  3.3: 3297119uV
VRef 3.0: 2998535uV
VRef 1.5: 1500000uV
USB-:    -4776400uV

Using calibration from: UNCALIBRATED
Unable to connect to WiFi AP. Error 0xA000001B

Enter the number of the operation you would like to do:
1. Enter JSON mode
2. Calibrate the instruments
3. Save the current calibration values
4. Manage your WiFi connections
5. View all files names on the SD card
6. View all files names in flash
7. Set the Oscilloscope input gain

{"mode":"JSON","statusCode":0,"wait":500}
{"device":[{"command":"calibrationLoad","statusCode":0,"wait":500}]}
{"awg":{"1":[{"command":"setRegularWaveform","statusCode":0,"actualSignalFreq":1000000,"actualVpp":3000,"actualVOffset":0,"wait":0}]},"osc":{"1":[{"command":"setParameters","statusCode":0,"actualVOffset":9,"actualSampleFreq":10000000,"actualTriggerDelay":0,"wait":0}]},"trigger":{"1":[{"command":"setParameters","statusCode":0,"wait":0},{"command":"single","statusCode":0,"acqCount":1,"wait":-1}]}}
{"awg":{"1":[{"command":"getCurrentState","statusCode":0,"state":"stopped","waveType":"sine","actualSignalFreq":1000000,"actualVpp":3000,"actualVOffset":0,"wait":0}]},"osc":{"1":[{"command":"getCurrentState","statusCode":0,"acqCount":0,"actualVOffset":9,"actualSampleFreq":10000000,"actualGain":1,"triggerDelay":0,"actualTriggerDelay":0,"actualBufferSize":30000,"state":"armed","wait":0}]}}
{"awg":{"1":[{"command":"run","statusCode":0,"wait":500}]}}
{"awg":{"1":[{"command":"getCurrentState","statusCode":0,"state":"running","waveType":"sine","actualSignalFreq":1000000,"actualVpp":3000,"actualVOffset":0,"wait":0}]},"osc":{"1":[{"command":"getCurrentState","statusCode":0,"acqCount":0,"actualVOffset":9,"actualSampleFreq":10000000,"actualGain":1,"triggerDelay":0,"actualTriggerDelay":0,"actualBufferSize":30000,"state":"armed","wait":0}]},"trigger":{"1":[{"command":"getCurrentState","statusCode":0,"acqCount":1,"state":"armed","source":{"instrument":"osc","channel":1,"type":"risingEdge","lowerThreshold":0,"upperThreshold":100},"targets":{"osc":[1]},"wait":0}]}}
{"osc":{"1":[{"command":"read","statusCode":2684354571,"wait":0}]}}

