@echo off
if not exist %CD%\build ( 
	echo build folder not found. Creating build folder.
	mkdir %CD%\build
	exit
)

if exist %CD%\build ( 
	echo build folder found. Cleaning build folder...
)

rmdir "%CD%\build" /s /q
if not exist %CD%\build ( 
	echo OK build folder deleted.
)
if exist %CD%\build ( 
	echo error couldn't delete!
	exit
)

mkdir "%CD%\build" 

if exist %CD%\build ( 
	echo Empty build folder created.
)