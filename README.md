# OSenergpatch
OpenScope source code with configuration files for building and uploading firmware using vscode

Use Windows ! 

Install vscode addon : Tasks https://marketplace.visualstudio.com/items?itemName=actboy168.tasks 

Install arduino 1.6.9 at %PROGRAMFILES(X86)%\Arduino : https://www.arduino.cc/en/main/OldSoftwareReleases

Install digilent core : https://reference.digilentinc.com/learn/software/tutorials/digilent-core-install/start

Install pyserial : `pip install pyserial`

Clone this repository `git clone https://github.com/ravikumargrk/OSenergpatch.git OSEP`

Use the task buttons on the taskbar to Build, Upload etc.

